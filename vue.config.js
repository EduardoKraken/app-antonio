const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: [
    'vuetify'
  ],

  // Modo web
  // publicPath: process.env.NODE_ENV === 'production' ? '/lms/' : '/', 
  
  // Modo movil
  publicPath: process.env.NODE_ENV === 'production' ? '' : '/',
})
