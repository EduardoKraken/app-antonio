import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import VueResource from 'vue-resource'
import Embed from 'v-video-embed'
// import './registerServiceWorker'
// import SoftUIDashboard from "./soft-ui-dashboard";
// Archivos css para las escuelas
import "./assets/css/escuela_css.css";

Vue.config.productionTip = false
Vue.use(VueResource)
Vue.use(Embed);

Vue.http.interceptors.push((request, next) => {
  request.headers.set('Accept', 'application/json')
  request.headers.set('Access-Control-Allow-Origin', '*')
  next()
});

// Vue.use(SoftUIDashboard);

if(process.env.NODE_ENV == 'development'){
    Vue.http.options.root = process.env.VUE_APP_RUTA_API_FAST;
  // Vue.http.options.root = 'http://localhost:3013/'
}else{
  if( store.state.ESCUELA == 1 ){
    Vue.http.options.root = process.env.VUE_APP_RUTA_API_INBI;
  }else{
    Vue.http.options.root = process.env.VUE_APP_RUTA_API_FAST;
  }
}


  // Vue.http.options.root = process.env.VUE_APP_RUTA_API_INBI;


new Vue({
    router,
    store,
    vuetify,
    render: function(h) { return h(App) }
}).$mount('#app')