import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/HomeView.vue'
import store from '@/store'

// Usuarios
import Login           from '@/views/usuarios/Login.vue'
import Perfil          from '@/views/usuarios/Perfil.vue'
import PerfilTeacher   from '@/views/usuarios/PerfilTeacher.vue'


// Componentes
import Carga           from '@/views/usuarios/Carga.vue'

// Alumnos
import DetalleGrupo    from '@/views/alumnos/DetalleGrupo.vue'  
import GruposAlumnos   from '@/views/alumnos/GruposAlumnos.vue'  
import Calificaciones  from '@/views/alumnos/Calificaciones.vue'  
import Kardex          from '@/views/alumnos/Kardex.vue'  
import Certificados    from '@/views/alumnos/Certificados.vue'  
import RegistrarDatos  from '@/views/alumnos/RegistrarDatos.vue'  

// Recursos
import RecursosGrupoUsuario   from '@/views/recursos/RecursosGrupoUsuario.vue'  
import Books                  from '@/views/recursos/Books.vue'  
import Ejercicio              from '@/views/recursos/Ejercicio.vue'  
import Examen                 from '@/views/recursos/Examen.vue'  
import Videos                 from '@/views/recursos/Videos.vue'  

// Soporte
import RecuperarPassword      from '@/views/soporte/RecuperarPassword.vue'  
import ReenviarCorreo         from '@/views/soporte/ReenviarCorreo.vue'  

// Grupos 
import GruposTeacher          from '@/views/grupos/GruposTeacher.vue'  
import DetalleGrupoTeacher    from '@/views/grupos/DetalleGrupoTeacher.vue'  
import ListaAlumnos           from '@/views/grupos/ListaAlumnos.vue'  
import ListaAsistencia        from '@/views/grupos/ListaAsistencia.vue'  
import RecursoGrupo           from '@/views/grupos/RecursoGrupo.vue'  
import CalificacionGrupo      from '@/views/grupos/CalificacionGrupo.vue' 

// notificaciones
import PruebaNotificacion     from '@/components/notificaciones/PruebaNotificacion.vue'  


Vue.use(VueRouter)

const router = new VueRouter({
  mode: '',
  base: process.env.BASE_URL,
  routes: [

    { path: '/pruebanoti'          , name: 'PruebaNotificacion'     , component: PruebaNotificacion, 
      meta: { libre: true }},

    { path: '/'          , name: 'Carga'     , component: Carga, 
      meta: { libre: true }},

    { path: '/login'     , name: 'Login'     , component: Login, 
      meta: { libre: true }},

    /*********************************************************************/

    { path: '/detallegrupo'    , name: 'DetalleGrupo'    , component: DetalleGrupo, 
      meta: { ALUMNO: true }},

    { path: '/gruposalumnos'   , name: 'GruposAlumnos'   , component: GruposAlumnos, 
      meta: { ALUMNO: true }},

    { path: '/calificaciones'  , name: 'Calificaciones'  , component: Calificaciones, 
      meta: { ALUMNO: true }},

    { path: '/kardex'          , name: 'Kardex'          , component: Kardex, 
      meta: { ALUMNO: true }},

    { path: '/certificados'    , name: 'Certificados'    , component: Certificados, 
      meta: { ALUMNO: true }},

    { path: '/registrardatos'  , name: 'RegistrarDatos'  , component: RegistrarDatos, 
      meta: { ALUMNO: true }},


    /*********************************************************************/

    { path: '/recursosgrupousuario'  , name: 'RecursosGrupoUsuario'  , component: RecursosGrupoUsuario, 
      meta: { ALUMNO: true }},

    { path: '/books'                 , name: 'Books'                 , component: Books, 
      meta: { ALUMNO: true }},

    { path: '/ejercicio'             , name: 'Ejercicio'             , component: Ejercicio, 
      meta: { ALUMNO: true }},

    { path: '/examen'                , name: 'Examen'                , component: Examen, 
      meta: { ALUMNO: true }},

    { path: '/videos'                , name: 'Videos'                , component: Videos, 
      meta: { ALUMNO: true }},

    /*********************************************************************/
    
    { path: '/perfil'           , name: 'Perfil'             , component: Perfil, 
      meta: { ADMIN: true, ALUMNO: true}},

    { path: '/home'             , name: 'Home'               , component: Home, 
      meta: { ADMIN: true, ALUMNO: true }},

    { path: '/perfilteacher'    , name: 'PerfilTeacher'      , component: PerfilTeacher, 
      meta: { ADMIN: true, TEACHER: true}},

    /*********************************************************************/

    { path: '/recuperarpassword' , name: 'RecuperarPassword'  , component: RecuperarPassword, 
      meta: { libre: true }},

    { path: '/reenviarcorreo'    , name: 'ReenviarCorreo'     , component: ReenviarCorreo, 
      meta: { libre: true }},

    /*********************************************************************/

    { path: '/gruposteacher'        , name: 'GruposTeacher'          , component: GruposTeacher, 
      meta: { ADMIN: true, TEACHER: true}},

    { path: '/detallegrupoteacher'  , name: 'DetalleGrupoTeacher'    , component: DetalleGrupoTeacher, 
      meta: { ADMIN: true, TEACHER: true}},

    { path: '/listaalumnos'         , name: 'ListaAlumnos'           , component: ListaAlumnos, 
      meta: { ADMIN: true, TEACHER: true}},

    { path: '/listaasistencia'      , name: 'ListaAsistencia'        , component: ListaAsistencia, 
      meta: { ADMIN: true, TEACHER: true}},

    { path: '/recursogrupo'         , name: 'RecursoGrupo'           , component: RecursoGrupo, 
      meta: { ADMIN: true, TEACHER: true}},

    { path: '/calificaciongrupo'    , name: 'CalificacionGrupo'      , component: CalificacionGrupo, 
      meta: { ADMIN: true, TEACHER: true}},

    /*********************************************************************/
      
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.libre)) {
    next()
  } else if (store.state.login.usuario.roll == 'ADMINISTRADOR') {
    if (to.matched.some(record => record.meta.ADMIN)) {
      next()
    }
  } else if (store.state.login.usuario.roll == 'ALUMNO') {
    if (to.matched.some(record => record.meta.ALUMNO)) {
      next()
    }
  } else if (store.state.login.usuario.roll == 'TEACHER') {
    if (to.matched.some(record => record.meta.TEACHER)) {
      next()
    }
  } else if (store.state.login.usuario.roll == 'EDITOR') {
    if (to.matched.some(record => record.meta.EDITOR)) {
      next()
    }
  } else {
    next({
      name: 'Login'
    })
  }
})

export default router